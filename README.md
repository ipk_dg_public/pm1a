# README #

R and Z shell scripts used in the analysis of a crop-wild introgression line in bread wheat.

* Binning the number of mapped reads in 1 Mb bins: `binning.zsh`

* Converting a VCF file into a long format table: `normalize.awk` and `normalize.zsh`

* Plotting SNP and read counts along the genome: `plots.R`

* Table of chromosome lengths of the bread wheat IWGSC RefSeq V1 assembly (DOI: 10.1126/science.aar7191): `161010_CS_v1.0_pseudomolecules_lengths.Rdata`

Further explanation is given in comment lines in the respective files.
